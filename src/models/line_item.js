import { Tween, autoPlay } from "es6-tween";
import { GAME_TOP_Y } from "../constant";
import { getSpriteFromCache } from "../utils/utils";
import GameObject from "./game_object";

export const LineItemEvent = Object.freeze({
    Complete: "lineitem:complete",
});

export default class LineItem extends GameObject {
    constructor(isVertical = true) {
        super();
        this.isVertical = isVertical;
        if (isVertical) {
            this.sprite = getSpriteFromCache("line_vertical");
        } else {
            this.sprite = getSpriteFromCache("line_horizontal");
        }
        this.addChild(this.sprite);
        this.alpha = 0.5
        autoPlay(true);
    }

    setPosition(value) {
        this.x = value;
        this.y = value;
        if (this.isVertical) {
            this.y = GAME_TOP_Y;
        } else {
            this.x = 0;
        }
        super.setPosition(this.x, this.y);
        this.runAnimation();
    }

    runAnimation() {
        this._initAnimation();
    }

    onComplete() {
        this.emit(LineItemEvent.Complete, this);
    }

    _initAnimation() {
        if (this.tween) 
            if (this.tween._isPlaying){
                this.tween.stop();
                if (this.isVertical) {
                    this.sprite.width = 5;
                } else
                    this.sprite.height = 5;
            }
        this.tween = new Tween({ thickness: 11 });
        this.tween
            .to({ thickness:0 }, 200)
            .on("update", ({thickness}) => {
                // console.log(e)
                if (this.isVertical) {
                    this.sprite.width = thickness;
                } else this.sprite.height = thickness;
            })
            .on("complete", this.onComplete, this)
            .on("stop", this.onComplete, this)
            .start();
    }
}
