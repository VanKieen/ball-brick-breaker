import GameObject, { GameObjectEvent } from "./game_object.js";
import * as Constant from "../constant";
import {
    randomInt,
    getSpriteFromCache,
    getShadowTextStyle,
} from "../utils/utils.js";
import { Text } from "pixi.js";
import RectCollider from "../collision/rect_collider.js";
import { Ball } from "./ball.js";
import { ColliderEvent } from "../collision/collider.js";
import { Tween, autoPlay } from "es6-tween";

export default class Brick extends GameObject {
    constructor(weight, color = undefined) {
        super();

        this.weight = weight;
        this.type = "brick";

        this.listColors = ["blue", "green", "orange", "purple"];
        if (!color)
            this.color = this.listColors[this.weight % this.listColors.length];
        else this.color = color;

        this.setSprite();

        this.textStyle = getShadowTextStyle(16);
        this.text = new Text(this.weight, this.textStyle);
        this.text.anchor.set(0.5, 0.5);
        this.isColliding = false;

        this.addChild(this.sprite);
        this.addChild(this.text);

        this.collider = new RectCollider(
            this.x,
            this.y,
            this.sprite.width,
            this.sprite.height
        );

        this.collider.on(ColliderEvent.Colliding, this.onCollision, this)

    }

    setPositionOnGrid(row, col, needUpdate = true) {
        let rs = super.setPositionOnGrid(row, col, needUpdate);
        (this.collider.x = this.x), (this.collider.y = this.y);
        return rs;
    }

    setPosition(x, y){
        super.setPosition(x, y);
        (this.collider.x = this.x), (this.collider.y = this.y);
    }

    setSprite() {
        if (this.sprite) {
            this.sprite.texture = getSpriteFromCache(
                `brick_${this.color}`
            ).texture;
        } else {
            this.sprite = getSpriteFromCache(`brick_${this.color}`);
        }
        this.sprite.width = Constant.BLOCK_SIZE;
        this.sprite.height = Constant.BLOCK_SIZE;
    }

    draw() {}

    onCollision() {
        if (this.weight > 0) {
            if(this.tween){
                this.tween.restart();
            }
            else{
                autoPlay(true);
                this.tween = new Tween({scale:1.4})
                .to({scale:1}, 100)
                .on('update', ({scale}) => {
                    this.text.scale.set(scale)
                })
                
            }

            this.updateWeight(this.weight-1)
        }
        
    }

    updateWeight(weight) {
        this.weight = weight;
        this.color = this.listColors[this.weight % this.listColors.length];
        this.setSprite();
        this.text.text = this.weight;
        if(this.weight === 0)
            this.emit(GameObjectEvent.NeedRemove, this)
    }

    onClick(fn) {
        this.addEventListener("touchstart", fn);
        this.addEventListener("click", fn);
    }

    getJson() {
        return {
            type: "brick",
            data: {
                row: this.row,
                col: this.col,
                x: this.x,
                y: this.y,
                weight: this.weight,
                size: this.sprite.width,
            },
        };
    }

    copy() {
        return new Brick(this.weight, this.color);
    }
}
