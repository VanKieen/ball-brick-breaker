import { Container } from "pixi.js";
import LineItem, { LineItemEvent } from "../models/line_item";
import { getSpriteFromCache } from "../utils/utils";

export default class LineManager extends Container {
    constructor() {
        super();
        this.listLine = [];
    }

    verticalAt(x) {
        console.log(`vertical ${x}`)
        let hasRan = false;
        for (var i = 0; i < this.listLine.length; i++) {
            let line = this.listLine[i];
            if (line.x === x) {
                line.runAnimation();
                hasRan = true;
            }
        }
        if (!hasRan) {
            let line = new LineItem();
            line.setPosition(x);
            this.addLine(line);
        }
    }

    horizontalAt(y) {
        console.log(`horizontal ${y}`)
        let hasRan = false;
        for (var i = 0; i < this.listLine.length; i++) {
            let line = this.listLine[i];
            if (line.y === y) {
                line.runAnimation(false);
                hasRan = true;
            }
        }
        if (!hasRan) {
            let line = new LineItem(false);
            line.setPosition(y);
            this.addLine(line);
        }
    }

    addLine(line) {
        this.listLine.push(line);
        this.addChild(line);
        line.on(LineItemEvent.Complete, () => {
            this.remove(line);
        });
    }

    remove(line) {
        let index = this.listLine.indexOf(line);
        if (index !== -1) {
            this.listLine.splice(index, 1);
            this.removeChild(line);
        }
    }
}
