import { Container, Text } from "pixi.js";
import {
    APP_HEIGHT,
    BLOCK_SIZE,
    DEFAULT_PADDING,
    GAME_HEIGHT,
    GAME_TOP_Y,
    GAME_WIDTH,
} from "../constant";
import { Ball, BallEvent } from "../models/ball";
import GameObject from "../models/game_object";
import { calcDistance, getShadowTextStyle } from "../utils/utils";
import { Tween, autoPlay } from "es6-tween";

export const BallManagerEvent = Object.freeze({
    Stop: "ballmanager:stop",
});

export default class BallManager extends Container {
    constructor(numOfBall) {
        super();
        this.numOfBall = numOfBall;

        this._initListBalls();
        this._initText();
    }

    _initListBalls() {
        this.rootBall = new Ball(BLOCK_SIZE / 4);
        this.addChild(this.rootBall);
        this.rootBall.setPosition(
            GAME_WIDTH / 2,
            GAME_HEIGHT - this.rootBall.radius - DEFAULT_PADDING * 2
        );

        this.listBalls = [];
        for (var i = 0; i < this.numOfBall; i++) {
            let ball = new Ball(BLOCK_SIZE / 4);
            ball.visible = false;
            this.listBalls.push(ball);
            ball.setPosition(this.rootBall.x, this.rootBall.y);
            this.addChild(ball);
            ball.on(BallEvent.Stop, this._moveToFirstBall, this);
        }

        this.numOfStopBall = this.numOfBall;
    }

    _initText() {
        this.txtNumBall = new Text(
            `x${this.numOfBall}`,
            getShadowTextStyle(13)
        );
        this.addChild(this.txtNumBall);
        this.txtNumBall.anchor.set(0.5, 0.5);
        this.txtNumBall.position.set(
            this.rootBall.x,
            this.rootBall.y - this.rootBall.radius - DEFAULT_PADDING * 3
        );
    }

    update(delta) {
        for (let i = 0; i < this.listBalls.length; i++) {
            let ball = this.listBalls[i];
            ball.update(delta);
            
        }
    }

    // cancel move by user
    stopMove() {
        let firstBall = this.listBalls[0];
        let numOfBallStopped = 1;
        let endPoint = {
            x: firstBall.x,
            y: APP_HEIGHT - GAME_TOP_Y - 5 * DEFAULT_PADDING,
        };
        
        autoPlay(true);
        new Tween({ x: firstBall.x, y: firstBall.y })
            .to(endPoint, 200)
            .on("update", ({ x, y }) => {
                firstBall.position.set(x, y);
            })
            .on("complete", () => {
                firstBall.setStop(endPoint.x, endPoint.y, true);
            })
            .start();
        

        this.listBalls.forEach((ball) => {
            if (ball !== firstBall) {
                let tween = new Tween({ x: ball.x, y: ball.y });
                tween.onStop = () =>{
                    numOfBallStopped++;
                        console.log(`ball ${numOfBallStopped} stopped`)
                        if(numOfBallStopped === this.numOfBall){
                            this.onShootDone()
                        }
                }

                tween
                    .to(endPoint, 200)
                    .on("update", ({ x, y }) => {
                        ball.position.set(x, y);
                    })
                    .on('complete', ({x, y}) => {
                        ball.setStop(x, y, true);
                        numOfBallStopped++;
                        if(numOfBallStopped === this.numOfBall){
                            this.onShootDone()
                        }
                    })
                    .start();
            }
        });
    }

    shoot(radian) {
        let lastBall = this.listBalls[0];
        lastBall.moving = true;
        lastBall.visible = true;
        lastBall.calcVelocity(radian);
        this.lastRadian = radian;

        this.numOfStopBall = 1;

        let index = 0;

        let intervalShoot = setInterval(() => {
            if (index < this.listBalls.length - 1) {
                let nextBall = this.listBalls[index + 1];
                let distance = calcDistance(
                    nextBall.x,
                    nextBall.y,
                    lastBall.x,
                    lastBall.y
                );
                // wait the previous ball move about 3* radius
                if (distance > lastBall.radius * 3) {
                    this.txtNumBall.text = `x${this.numOfBall - index - 2}`;
                    lastBall = nextBall;
                    lastBall.moving = true;
                    lastBall.visible = true;
                    lastBall.calcVelocity(radian);
                    index++;
                }
            } else {
                clearInterval(intervalShoot);
            }
        }, 100);
    }

    _moveToFirstBall(ball) {
        let firstBall = this.listBalls[0];
        if (firstBall.vx === 0 && firstBall.vy === 0) {
            if (firstBall.moving) {
                firstBall.setStop();
                firstBall.visible = true;
            }

            let distance = calcDistance(
                ball.x,
                ball.y,
                firstBall.x,
                firstBall.y
            );

            if (ball.moved) {
                ball.y = firstBall.y;
                if (distance > 2 * ball.radius) {
                    if (ball.x < firstBall.x) ball.vx = ball.speed;
                    else ball.vx = -ball.speed;
                } else {
                    ball.setStop(firstBall.x, firstBall.y);
                    this.onBallStop(ball);
                }
            }
        }
    }

    onBallStop(ball) {
        this.numOfStopBall++;
        if (this.numOfStopBall === this.numOfBall) {
            this.onShootDone();
        }
    }

    onShootDone() {
        this.rootBall.setPosition(this.listBalls[0].x, this.listBalls[0].y);
        this.txtNumBall.text = `x${this.numOfBall}`;
        this.txtNumBall.position.set(
            this.rootBall.x,
            this.rootBall.y - this.rootBall.radius - DEFAULT_PADDING * 3
        );
        this.emit(BallManagerEvent.Stop);
    }

    addBall() {
        let ball = this.listBalls[this.listBalls.length - 1].copy();
        this.numOfBall++;
        this.listBalls.push(ball);
        this.addChild(ball);
        ball.calcVelocity(this.lastRadian);
        ball.on(BallEvent.Stop, this._moveToFirstBall, this);
        return ball;
    }

    setItemActivated(itemWork, collider) {
        this.listBalls.forEach((ball) => {
            if (ball.collider === collider) {
                ball.setItemActivated(itemWork);
                return;
            }
        });
    }
}
