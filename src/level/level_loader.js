import { EventEmitter } from "@pixi/utils";
import LevelFile from "../../assets/levels/level.json"
import Level from "./level";

export const LevelLoaderEvent = Object.freeze({
  Load: "levelloader:load",
});

export default class LevelLoader extends EventEmitter {
  constructor() {
    super();
  }

  load() {
    LevelFile.forEach(file => {
      this._loadJSON(file, this._loadLevel.bind(this))
    })
  }

  _loadJSON(file, onLoad) {
    const data = require(`../../assets/levels/${file}`);
    onLoad(data)
  }

  _loadLevel(data) {
    let level = new Level(data);
    this.emit(LevelLoaderEvent.Load, level);
  }
}