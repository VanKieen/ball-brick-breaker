import { Container } from "pixi.js";
import { LevelEvent } from "./level";

export const LevelManagerEvent = Object.freeze({
  Start:    "levelmanager:start",
  Complete: "levelmanager:complete",
  Finish:   "levelmanager:finish",
  GameOver: "levelmanager:gameover"
});

export default class LevelManager extends Container {
  constructor() {
    super();

    /** @type {Array<Level>} */
    this.levels = [];
    this.curLevelIndex = 0;
    
  }

  addLevel(level) {
    this.levels.push(level);
  }

  start() {
    if (this.levels.length <= 0) {
      return;
    }

    this.startLevel(0);
  }

  startLevel(index) {
    this.curLevelIndex = index;
    if (this.curLevelIndex >= this.levels.length) {
      return;
    }

    let level = this.levels[this.curLevelIndex];
    level.on(LevelEvent.Complete, this.onLevelComplete, this);
    level.on(LevelEvent.Failure, this.onLevelFail, this);
    level.on(LevelEvent.Scored, (score) => this.emit(LevelEvent.Scored, score))

    this.addChild(level);

    this.emit(LevelManagerEvent.Start, level);

    this.curLevel = level;
  }

  nextLevel() {
    let curLevel = this.levels[this.curLevelIndex];
    this.removeChild(curLevel);

    this.startLevel(this.curLevelIndex + 1);
  }

  onLevelComplete(level) {
    this.emit(LevelManagerEvent.Complete, level);
  }

  onLevelFail(level){
    this.emit(LevelManagerEvent.GameOver, level)
  }

  update(delta){
    this.curLevel.update(delta);
  }
}