import { CIRCLE_COLLIDER, RECT_COLLIDER } from "../constant";
import Collider, { ColliderEvent } from "./collider";

export default class RectCollider extends Collider {
    constructor(x, y, width, height){
        super(RECT_COLLIDER, x, y,);
        this.width = width;
        this.height = height;
    }

    
    checkCollision(objectToCheck) {
        let isColliding;
        if (objectToCheck.type === CIRCLE_COLLIDER) {
            isColliding = objectToCheck.checkCollision(this);
        } else if (objectToCheck.type === RECT_COLLIDER) {
            isColliding = this.rectCollision(objectToCheck)
        }
        if (isColliding) {
            this.emit(ColliderEvent.Colliding, objectToCheck);
        }
        return isColliding;
    }

    rectCollision(object) {
        let distance = calcDistance(this.x, this.y, object.x, object.y);
        let widthDistance = object.width / 2 + this.width / 2;
        let heightDistance = object.height / 2 + this.height / 2;
        if (widthDistance < distance) {
            if (this.y <= object.y) {
                return "top";
            } else if (this.y >= object.y) {
                return "bottom";
            }
        } else if (heightDistance < distance) {
            if (this.x <= object.x) {
                return "left";
            } else if (this.x >= object.y) {
                return "right";
            }
        }
    }
}