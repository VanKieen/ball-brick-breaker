import * as PIXI from "pixi.js";

export function getSpriteFromCache(name){
    return new PIXI.Sprite(PIXI.utils.TextureCache[`${name}`]);
}

export function getShadowTextStyle(fontSize = 36){
    return new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: fontSize,
        fontWeight: 'bold',
        fill: ['#ffffff'],
        dropShadow: true,
        dropShadowColor: '#000000',
        dropShadowBlur: 3,
        dropShadowAngle: Math.PI / 4,
        dropShadowDistance: 4,
    });
}

export function downloadFile(name, contents, mime_type) {
    mime_type = mime_type || "text/plain";

    var blob = new Blob([contents], {type: mime_type});

    var dlink = document.createElement('a');
    dlink.download = name;
    dlink.href = window.URL.createObjectURL(blob);
    dlink.onclick = function(e) {
        // revokeObjectURL needs a delay to work properly
        var that = this;
        setTimeout(function() {
            window.URL.revokeObjectURL(that.href);
        }, 1500);
    };

    dlink.click();
    dlink.remove();
}

export function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function calcDistance(x1, y1, x2 = undefined, y2 = undefined) {
    if(x2 && y2) {
        return Math.sqrt((x2-x1)**2 + (y2-y1)**2);
    }
    else{
        return Math.sqrt(x1**2 + y1**2);
    }
}

export function calcVector(x1, y1, x2, y2){
    return {
        x: x2-x1,
        y: y2-y1
    }
}

export function calcAngle(x1, y1, x2, y2){
    let dot = dotProduct(x1, y1, x2, y2);
    let d1 = calcDistance(x1, y1);
    let d2 = calcDistance(x2, y2);
    let cosAlpha = dot/(d1*d2);
    return Math.acos(cosAlpha)
}

export function dotProduct(x1, y1, x2, y2) {
    return x1*x2+y1*y2;
}

export function rotate(x, y, theta){
    return {
        x: x * Math.cos(theta) - y * Math.sin(theta),
        y: x * Math.sin(theta) + y * Math.cos(theta),
    }
}

 // line intercept math by Paul Bourke http://paulbourke.net/geometry/pointlineplane/
// Determine the intersection point of two line segments
// Return FALSE if the lines don't intersect
export function intersect(x1, y1, x2, y2, x3, y3, x4, y4) {

  // Check if none of the lines are of length 0
	if ((x1 === x2 && y1 === y2) || (x3 === x4 && y3 === y4)) {
		return false
	}

	let denominator = ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1))

  // Lines are parallel
	if (denominator === 0) {
		return false
	}

	let ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denominator
	let ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denominator

  // is the intersection along the segments
	if (ua < 0 || ua > 1 || ub < 0 || ub > 1) {
		return false
	}

  // Return a object with the x and y coordinates of the intersection
	let x = x1 + ua * (x2 - x1)
	let y = y1 + ua * (y2 - y1)

	return {x, y}
}
